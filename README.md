# pytodo - A Python library helping you keep track of tasks

## Ambition

* A local, offline library to provide task tracking in a similar fashion to
  Atlassian Jira.

## Design Ideas

* I hate databases, so I'm not doing them. The library will open `json` files
  and dump to an object. This will then be edited by whatever (CLI initially),
  and then dumped back to file on program shutdown.
* Tasks should be storable (pullable) from more than one file?
* Task categories should be lists within the data structure, so searching a
  "category" field isn't necessary on load.

## JSON Format

```python
{
  "categories": [
    {
      "name": "",
      "tasks": [
        {
          "description": "",
          "action": "",
          "date_created": "",
          "date_last_updated": "",
          "status": "",
          "status_comment": "",
          "keywords": [],
          "notes": ""
        }
      ]
    }
  ]
}
```
