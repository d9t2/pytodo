#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


def lower_snake_case_to_title(field: str) -> str:
    """Take a lower snake case word and capitalise it with spaces."""
    return " ".join([word.capitalize() for word in field.split("_")])


def left_pad(word: str, length: int, pad_char: str = " ") -> str:
    """Left align a string with characters up to a given length."""
    return word + (pad_char * (length - len(word)))


def centre_pad(word: str, length: int, pad_char: str = " ") -> str:
    """Centre align a string with characters up to a given length."""
    left = (length - len(word)) // 2
    if left * 2 + len(word) != length:
        right = left + 1
    else:
        right = left
    return (pad_char * left) + word + (pad_char * right)


def print_table(table: list[list],
                horizontal_char: str = "=",
                vertical_char: str = "|",
                intersection_char: str = "+"):
    """Print a 2D list as a table."""
    if len(table) == 0:
        raise ValueError("Can't print an empty table")
    # Convert whole table to strings
    table = [[str(cell) if cell is not None else ""
              for cell in row]
             for row in table]
    # Get maximum content length per column
    lengths = [len(cell) for cell in table[0]]
    for i, row in enumerate(table[1:]):
        for j, cell in enumerate(row):
            if len(cell) > lengths[j]:
                lengths[j] = len(cell)
    # Pad cells
    table = [[left_pad(cell, lengths[j])
              for j, cell in enumerate(row)]
             for i, row in enumerate(table)]
    # Generate border strings
    row_border = (
        intersection_char + horizontal_char +
        (f"{horizontal_char}{intersection_char}{horizontal_char}".join([
            (horizontal_char * length) for length in lengths])) +
        horizontal_char + intersection_char)
    print(row_border)
    print(vertical_char + " " +
          (f" {vertical_char} ".join([cell for cell in table[0]])) +
          " " + vertical_char)
    print(row_border)
    for row in table[1:]:
        print(vertical_char + " " +
              (f" {vertical_char} ".join([cell for cell in row])) +
              " " + vertical_char)
    print(row_border)
