#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Union

from .formatting import lower_snake_case_to_title
from .task import Task


Tasks = dict[str, Task]
TaskList = list[Task]
TasksUnion = Union[None, list[dict], TaskList, Tasks]


def tasks_from_tasks_union(tasks: TasksUnion,
                           category: str) -> Tasks:
    """Convert TasksUnion to Tasks if necessary and possible."""
    if tasks is None:
        return None
    if isinstance(tasks, list):
        if len(tasks) == 0:
            return {}
        if isinstance(tasks[0], dict):
            tasks = [Task.from_dictionary(task, category) for task in tasks]
        return {task.description: task for task in tasks}
    if isinstance(tasks, dict):
        return tasks
    raise TypeError(f"`tasks` is invalid type: {type(tasks)}")


def task_list_from_tasks(tasks: Tasks) -> TaskList:
    """Convert Tasks to TaskList."""
    return [task.to_dictionary() for _, task in tasks.items()]


class Category:
    """Represents a category of tasks."""

    def __init__(self, name: str, tasks: Tasks = {}):
        """Initialise the category object."""
        self.name = name
        self.tasks = tasks_from_tasks_union(tasks, name)

    @classmethod
    def from_dictionary(cls, dct: dict):
        """Create a `Category` object from a dictionary."""
        name = dct.get("name")
        if name is None:
            raise ValueError(f"Dictionary doesn't contain 'name': {dct}")
        tasks = dct.get("tasks", [])
        return cls(name, tasks)

    def to_dictionary(self) -> dict:
        """Return the dictionary representation of this object."""
        return {
            "name": self.name,
            "tasks": task_list_from_tasks(self.tasks)}

    def __str__(self) -> str:
        """Return the string representation of this object."""
        return str(self.to_dictionary())

    def new_task(self, task: Task):
        """Create a new task."""
        self.tasks[task.description] = task
        return task

    def to_table(self) -> list[list[str]]:
        """Convert a Category object to a 2D list, to be viewed as a table."""
        if len(self.tasks) == 0:
            return []
        # Get maximal title list
        fields = set()
        for task in self.tasks.values():
            fields |= set(task.field_names())
        # Get rows
        ret = [list([lower_snake_case_to_title(field) for field in fields])]
        for task in self.tasks.values():
            dct = task.to_dictionary()
            ret.append([dct.get(field) for field in fields])
        return ret
