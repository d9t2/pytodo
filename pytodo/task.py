#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from datetime import date as Date
from typing import Union


DateUnion = Union[None, str, Date]


def date_from_union_date(date: DateUnion) -> Date:
    """Convert DateUnion to datetime.Date if necessary and possible."""
    if date is None:
        return None
    if isinstance(date, str):
        return Date.fromisoformat(date)
    if isinstance(date, Date):
        return date
    raise TypeError(f"`date` is invalid type: {type(date)}")


def str_from_union_date(date: DateUnion) -> str:
    """Convert DateUnion to str."""
    if date is None:
        return ""
    if isinstance(date, str):
        return date
    if isinstance(date, Date):
        return str(date)
    raise TypeError(f"`date` is invalid type: {type(date)}")


class Task:
    """Represents a single task."""

    def __init__(self,
                 description: str,
                 action: str,
                 category: str,
                 date_created: DateUnion = Date.today(),
                 date_last_updated: DateUnion = Date.today(),
                 date_due: DateUnion = None,
                 status: str = None,
                 status_comment: str = None,
                 keywords: [str] = None,
                 notes: str = None):
        """Initialise the object."""
        self.description = description
        self.action = action
        self.category = category
        self.date_created = date_from_union_date(date_created)
        self.date_last_updated = date_from_union_date(date_last_updated)
        self.date_due = date_from_union_date(date_due)
        self.status = status
        self.status_comment = status_comment
        self.keywords = keywords
        self.notes = notes

    @classmethod
    def from_dictionary(cls, dct: dict, category: str):
        """Create a `Task` object from a dictionary."""
        description = dct.get("description")
        if description is None:
            raise ValueError("Dictionary doesn't contain 'description': "
                             f"{dct}")
        action = dct.get("action")
        if action is None:
            raise ValueError(f"Dictionary doesn't contain 'action': {dct}")
        date_created = dct.get("date_created")
        date_last_updated = dct.get("date_last_updated")
        date_due = dct.get("date_due")
        status = dct.get("status")
        status_comment = dct.get("status_comment")
        keywords = dct.get("keywords")
        notes = dct.get("notes")
        return cls(
            description,
            action,
            category,
            date_created,
            date_last_updated,
            date_due,
            status,
            status_comment,
            keywords,
            notes)

    def to_dictionary(self) -> dict:
        """Return the dictionary representation of this object."""
        ret = {
            "description": self.description,
            "action": self.action}
        if self.date_created is not None:
            ret["date_created"] = str_from_union_date(self.date_created)
        if self.date_last_updated is not None:
            ret["date_last_updated"] = (
                str_from_union_date(self.date_last_updated))
        if self.date_due is not None:
            ret["date_due"] = str_from_union_date(self.date_due)
        if self.status is not None:
            ret["status"] = self.status
        if self.status_comment is not None:
            ret["status_comment"] = self.status_comment
        if self.keywords is not None:
            ret["keywords"] = self.keywords
        if self.notes is not None:
            ret["notes"] = self.notes
        return ret

    def __str__(self) -> str:
        """Return the string representation of this object."""
        return str(self.to_dictionary())

    def field_names(self) -> [str]:
        """Return the fields that this task uses."""
        return list(self.to_dictionary().keys())
