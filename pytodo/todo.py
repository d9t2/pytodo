#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Union

from .category import Category
from .task import Task


Categories = dict[str, Category]
CategoryList = list[Category]
CategoriesUnion = Union[list[dict], CategoryList, Categories]


def categories_from_categories_union(
        categories: CategoriesUnion) -> Categories:
    """Convert CategoriesUnion to Categories if necessary and possible."""
    if categories is None:
        return None
    if isinstance(categories, list):
        if len(categories) == 0:
            return {}
        if isinstance(categories[0], dict):
            categories = [Category.from_dictionary(category)
                          for category in categories]
        return {category.description: category for category in categories}
    if isinstance(categories, dict):
        return categories
    raise TypeError(f"`categories` is invalid type: {type(categories)}")


def category_list_from_categories(categories: Categories) -> CategoryList:
    """Convert Categories to CategoryList."""
    return [category.to_dictionary() for _, category in categories.items()]


class Todo:
    """Represents the totality of all categories."""

    def __init__(self, categories: Categories = {}):
        """Initialise the categories object."""
        self.categories = categories_from_categories_union(categories)

    @classmethod
    def from_dictionary(cls, dct: dict):
        """Create a `Categories` object from a dictionary."""
        categories = dct.get("categories")
        if categories is None:
            raise ValueError(f"Dictionary doesn't contain 'categories': {dct}")
        return cls(categories)

    def to_dictionary(self) -> dict:
        """Return the dictionary representation of this object."""
        return {
            "categories": category_list_from_categories(self.categories)}

    def __str__(self) -> str:
        """Return the string representation of this object."""
        return str(self.to_dictionary())

    def new_category(self, name: str):
        """Create a new category."""
        category = Category(name)
        self.categories[name] = category
        return category

    def new_task(self, task: Task) -> Task:
        """Create a new task. Create a category for the task if required."""
        if task.category not in self.categories:
            self.new_category(task.category)
        return self.categories[task.category].new_task(task)

    def to_table(self) -> list[list[str]]:
        """Convert a Todo object to a 2D list, to be viewed as a table."""
        ret = []
        for category in self.categories.values():
            ret.extend(category.to_table())
